# Docker version of ocserv

Set hostname variable (SRV_CN) with exact data in start.sh then run script.

If no certs is presented in /etc/ocserv/certs/ then it generates selfsigned ones.

Ocserv starts with default user "test" password "test".
To add user run 'docker exec -ti ocserv ocpasswd -c /etc/ocserv/ocpasswd yourusername'
To remove test user run 'docker exec -ti ocserv ocpasswd -c /etc/ocserv/ocpasswd -d test'

Ocserv starts with camouflage enabled and camouflage_secret 'secretkey'.
To change camouflage secret run 'docker exec ocserv sed -i '/^camouflage_secret = /{s/secretkey/newsecretkey/}' /etc/ocserv/ocserv.conf'

Host requirements: certbot
